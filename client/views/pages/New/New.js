



Template.New.helpers({
  activeAccount: function() {
    let _id = Session.get("activeAccount")
    console.log(_id);
    
    if (_id === "-1") {
      return false
    } else {
      return Account.findOne({_id})
    }
  },
  dateFormat: (date)=>{
    return moment(date).format("DD/MM/yy")
  }
});

Template.New.events({ 
  'click [Save]': function(event, template) { 
    let data  = $(".form input").serializeJSON();
     Meteor.call('Save', data, function(error, success) { 
      error ? alert(error.message) : alert("Kayıt Edildi")
     });
  },
  'click [Delete]': function(event, template) { 
    Meteor.call('Delete', Session.get("activeAccount"), function(error, success) { 
      alert("İşlem Tamamlandı")
      Router.go('/Panel')
    });
  },
  'click [Back]': function(event, template) { 
    Router.go('/Panel')

  } 
});

Template.New.onRendered(function() {
  this.subscribe('Account',Session.get("activeAccount"));
});