


Template.Panel.helpers({
  fetchData: function(){
    return Account.find().fetch();
  }
});

Template.Panel.events({
  "click #logout": function(event){
    Meteor.logout();
    Router.go("/")
  },

  "click .list-group-item": function(event,tem){    
    Session.set("activeAccount", event.currentTarget.id)
    Router.go('/New')
  },
  "click [Go]": function(event){
    Router.go('/New')
  }
});


Template.Panel.onRendered(function ( ){
    this.subscribe('Accountes');
})